use criterion::{black_box, criterion_group, criterion_main, Criterion};
use thread_owned_lock::StdThreadOwnedLock;

fn bench_thread_owned_lock(c: &mut Criterion) {
    let mutex = StdThreadOwnedLock::new(20);
    c.bench_function("ThreadOwnedMutex::lock", |b| {
        b.iter(|| {
            let _guard = black_box(mutex.lock());
        })
    });
}

fn bench_std_lock(c: &mut Criterion) {
    let mutex = std::sync::Mutex::new(20);
    c.bench_function("Std::Mutex::lock", |b| {
        b.iter(|| {
            let _guard = black_box(mutex.lock().unwrap());
        })
    });
}

fn bench_parking_lot_lock(c: &mut Criterion) {
    let mutex = parking_lot::Mutex::new(20);
    c.bench_function("ParkingLot::Mutex::lock", |b| {
        b.iter(|| {
            let _guard = black_box(mutex.lock());
        })
    });
}

criterion_group!(
    benches,
    bench_thread_owned_lock,
    bench_std_lock,
    bench_parking_lot_lock
);
criterion_main!(benches);
